package tugasJava1;

import java.util.Arrays;

public class StackImpl implements Stacking{

	@Override
	public int[] push(int n, int[] arr, int x) {
		
        int newArr[] = new int[n + 1];
  
        for (int i = 0; i < n; i++)
            newArr[i] = arr[i];
  
        newArr[n] = x;
  
        return newArr;
	}

	@Override
	public int[] pop(int arr[]) {
		int[] newArr = Arrays.copyOf(arr, arr.length - 1);
		
		return newArr;
	}

	@Override
	public int max(int arr[]) {
		int max = arr[0];
		for(int num: arr  ) {
			if(max < num) {
				max = num;
			}
			
		}
		
		return max;
		
		
		
	}

	@Override
	public int size(int arr[]) {
		return arr.length;
		// TODO Auto-generated method stub
		
	}



	

}
