package tugasJava1;

import java.util.Arrays;

public class AnagramTask {
	
	public static void isAnagram(String s1, String s2){
	
	boolean status = true;
	
//	make sure string length is match
	
	if (s1.length()!= s2.length()) {
		status = false;
	}
	else {
		char[] arrayS1 = s1.toCharArray();
		char[] arrayS2 = s2.toCharArray();
		
		Arrays.sort(arrayS1);
		Arrays.sort(arrayS2);
		status = Arrays.equals(arrayS1, arrayS2);
		
		if (status) {
			System.out.println(s1+ " and "+ s2 + " are anagram" );
		}
		else 
		{
			System.out.println(s1+ " and "+ s2 + " are NOT anagram");
		}
		
	}


	}
	
	public static void main (String [] args) {	
		isAnagram("dusty", "study");
		isAnagram("bored", "robed");
		isAnagram("fluster", "restful");
		isAnagram("dodol", "lolod");
	 }
}
