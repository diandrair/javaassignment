package tugasJava1;

public class ReverseList {
	public static void main(String[] args) {
		int [] originalList = {1,2,3,4,5};
		
		System.out.println("Original List: ");
		for(int i=0; i< originalList.length; i++) {
			System.out.println(originalList[i]+"");
		}
		
		System.out.println("Reversed List: ");
		for(int i = originalList.length-1; i>=0; i--) {
			System.out.println(originalList[i]+"");
		}
	}

}
