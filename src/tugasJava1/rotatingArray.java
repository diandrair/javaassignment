package tugasJava1;

import java.util.Arrays;
import java.util.Scanner;

public class rotatingArray {
	
	
//	OriArray = {1,2,3,4,5}
	
//	arr[] = input array
//	d = jumlah rotate
//	n = length array
	
	
	
	public static void leftRotate(int arr[], int d, int n) {
//		create temporary array
		int newArr[] = new int [d];
		
//		copy element sebanyak d ke temporary array
		for (int i =0 ; i < d; i++)
			newArr[i] = arr[i];
		
//		moving rest element by index
		for (int i = d; i< n; i++)
			arr[i-d] = arr[i];
		
//		copy element dari temporary array ke original array dari belakang
		for (int i = 0; i < d; i++)
			arr[i+n-d]= newArr[i];
		System.out.println("number to rotate: "+ Arrays.toString(newArr));
	}
	

	public static void main (String[] args) {
		Scanner input = new Scanner(System.in);
		System.out.println("Input number of rotate :");
		int d = input.nextInt();
		
		int[] arr = {1,2,3,4,5};
		System.out.println("original array:" + Arrays.toString(arr));
		
		leftRotate(arr, d, arr.length);
		System.out.println("rotated array: "+ Arrays.toString(arr));
	}
}
